from django.apps import AppConfig


class SimpleBankAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'simple_bank_app'
