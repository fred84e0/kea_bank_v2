from django.http import JsonResponse
from django.shortcuts import render
from .models import Account, Debit, Credit, Ledger
from django.db.models import Sum
from django.db import transaction
import uuid

def index(request):
    if request.method == 'POST':
        account_name = request.POST['account_name']
        account = Account()
        account.account_name = account_name
        account.save()
    
    accounts = Account.objects.filter(user=request.user)
    ledger_count = Ledger.objects.all().aggregate(Sum('amount'))['amount__sum']
    
    context = {
        'accounts': accounts,
        'ledger_count' : ledger_count
    }
    return render(request, 'simple_bank_app/index.html', context)

def account_balance(request, account_id):
    credit = Credit.objects.filter(account_id=account_id).aggregate(Sum('amount'))['amount__sum']
    debit = Debit.objects.filter(account_id=account_id).aggregate(Sum('amount'))['amount__sum']
    if credit == None:
        credit = 0
    if debit == None:
        debit = 0
    balance = credit - debit
    context = {
        'account_id': account_id,
        'balance': balance
    }
    # return JsonResponse(context)
    return render(request, 'simple_bank_app/account.html', context)


def make_transaction(request):
    if request.method == 'POST':
        from_acc_id = request.POST['from_account']
        to_acc_id = request.POST['to_account']
        amount =  request.POST['amount']
        message =  request.POST['message']
        transaction_id = str(uuid.uuid4())

        from_acc = Account.objects.get(account_id=from_acc_id)
        to_acc = Account.objects.get(account_id=to_acc_id)

    
        with transaction.atomic():
            #Save the Debit instance
            debitor = Debit()
            debitor.amount = amount
            debitor.account_id = from_acc
            debitor.save()
            
            #Save the Credit instance
            creditor = Credit()
            creditor.amount = amount
            creditor.account_id = to_acc
            creditor.save()
            
            #Now add to ledger
            #-abs turn number into negative number
            debitor_ledger_instance = Ledger()
            debitor_ledger_instance.account_id = from_acc_id
            debitor_ledger_instance.amount = -abs(int(amount))
            debitor_ledger_instance.transaction_id = transaction_id
            debitor_ledger_instance.message = message
            debitor_ledger_instance.is_creditor = False
            debitor_ledger_instance.save()

            creditor_ledger_instance = Ledger()
            creditor_ledger_instance.account_id = to_acc_id
            creditor_ledger_instance.amount = int(amount)
            creditor_ledger_instance.transaction_id = transaction_id
            creditor_ledger_instance.message = message
            creditor_ledger_instance.is_creditor = True
            creditor_ledger_instance.save()

    return render(request, 'simple_bank_app/transaction.html')





