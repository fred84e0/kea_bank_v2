from django.contrib import admin
from .models import CustomUser, Account, Debit, Credit, Ledger

admin.site.register(CustomUser)
admin.site.register(Account)
admin.site.register(Debit)
admin.site.register(Credit)
admin.site.register(Ledger)

