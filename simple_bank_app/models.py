from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from django.utils import timezone


class CustomUser(AbstractUser):
    is_employee = models.BooleanField(default=False)
    rank = models.IntegerField(default=1)
    phone = PhoneNumberField()

class Account(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    account_name = models.CharField(max_length=200)
    account_id =  models.AutoField(primary_key=True)

    def __str__(self):
        return f'{self.user} - {self.account_name} - {self.account_id}'

class Debit(models.Model):
    account_id = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=40, decimal_places=2)

    def __str__(self):
        return f'{self.account_id} - {self.amount}'

class Credit(models.Model):
    account_id = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=40, decimal_places=2)

    def __str__(self):
        return f'{self.account_id} - {self.amount}'


class Ledger(models.Model):
    # account = models.ForeignKey(Account, on_delete=models.PROTECT, default=0)
    account_id = models.IntegerField(default=0)
    amount = models.DecimalField(max_digits=40, decimal_places=2)
    message = models.CharField(max_length=200)
    time = models.DateTimeField(default=timezone.now)
    transaction_id = models.CharField(max_length=100)
    is_creditor = models.BooleanField(default=False)

    def __str__(self):
        return f' - {self.amount} - {self.message} - {self.time}'