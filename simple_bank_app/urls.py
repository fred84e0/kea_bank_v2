from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('account/<int:account_id>/', views.account_balance, name='account'),
    path('transaction/', views.make_transaction, name='transaction'),
]